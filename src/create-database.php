<?php
declare(strict_types=1);

$host     = 'db.local';
$user     = 'root';
$database = 'dbtest';
$password = 'fdglkjfjklhdfg67';

// connect
try {
    $pdo = new PDO(sprintf('mysql:host=%s;charset=utf8', $host), $user, $password);
} catch (PDOException $e) {
    die(sprintf('Connection failed: %s', $e->getMessage()));
}

// create DB
try {
    $pdo->exec(sprintf('DROP DATABASE IF EXISTS %s', $database));
    $pdo->exec(sprintf('CREATE DATABASE IF NOT EXISTS %s', $database));
} catch (PDOException $e) {
    die(sprintf('Failed to create database "%s": %s', $database, $e->getMessage()));
}

die(sprintf('Successfully created database "%s"', $database));
